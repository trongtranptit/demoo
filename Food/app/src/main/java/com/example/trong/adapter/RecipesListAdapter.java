package com.example.trong.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.trong.food.DetailRecipesTabAvtivity;
import com.example.trong.food.R;
import com.example.trong.model.RecipesModel;

import java.util.ArrayList;

/**
 * Created by trong on 7/14/2017.
 */

public class RecipesListAdapter extends RecyclerView.Adapter<RecipesListAdapter.RecipesHolder> {
    Context ctx;
    ArrayList<RecipesModel> data;

    public int img[]={R.drawable.img1, R.drawable.img2, R.drawable.img3, R.drawable.img4
    , R.drawable.img5, R.drawable.img6};
    public RecipesListAdapter(Context ctx, ArrayList<RecipesModel> data) {
        this.ctx = ctx;
        this.data = data;
    }

    @Override
    public RecipesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.recipes_list_item, parent,false);

        return new RecipesHolder(v);
    }

    @Override
    public void onBindViewHolder(RecipesHolder holder, final int position) {
        holder.name1.setText(data.get(position).getName1());
        holder.name2.setText(data.get(position).getName2());
        holder.img1.setImageResource(img[position%6]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent t = new Intent(v.getContext(), DetailRecipesTabAvtivity.class);
                t.putExtra("name1",data.get(position).getName1());
                t.putExtra("name2",data.get(position).getName2());
                v.getContext().startActivity(t);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class RecipesHolder extends RecyclerView.ViewHolder {
        TextView name1, name2;
        ImageView img1;
        public RecipesHolder(View itemView) {
            super(itemView);
            name1 = (TextView)itemView.findViewById(R.id.tvName1);
            name2 = (TextView)itemView.findViewById(R.id.tvName2);
            img1 =(ImageView)itemView.findViewById(R.id.img1);
        }
    }
}
