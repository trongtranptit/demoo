package com.example.trong.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.trong.adapter.RecipesListAdapter;
import com.example.trong.food.R;
import com.example.trong.model.RecipesModel;

import java.util.ArrayList;

/**
 * Created by trong on 7/12/2017.
 */

public class RecipesFragment extends Fragment {
    RecyclerView recyclerView;
    ArrayList<RecipesModel> data = new ArrayList<RecipesModel>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_recipes,container,false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerRecipes);
        addData();
        RecipesListAdapter adapter = new RecipesListAdapter(v.getContext(),data);
        LinearLayoutManager layoutManager = new LinearLayoutManager(v.getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(false);
        return v;
    }

    private void addData() {
        for(int i=0;i<4;i++){
            RecipesModel model;
            model = new RecipesModel("Mandarian Sorbet", "by John Doe");
            data.add(model);
            model = new RecipesModel("BlackBerry Sorbet", "by Stansil Kirilov");
            data.add(model);
            model = new RecipesModel("Mango Sorbet", "by John Doe");
            data.add(model);
            model = new RecipesModel("StrawBerry Sorbet", "by Steve Thomas");
            data.add(model);
            model = new RecipesModel("Apple Sorbet", "by Mark Kratzman");
            data.add(model);
            model = new RecipesModel("Sweet Sorbet", "by Selene Setty");
            data.add(model);
        }
    }
}
