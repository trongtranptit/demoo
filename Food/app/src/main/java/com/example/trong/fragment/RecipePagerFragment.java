package com.example.trong.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TextView;

import com.example.trong.adapter.PagerAdapter;
import com.example.trong.food.R;

/**
 * Created by trong on 7/12/2017.
 */

public class RecipePagerFragment extends Fragment{
    FragmentTabHost mTabHost;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mTabHost = new FragmentTabHost(getActivity());
        mTabHost.setup(getActivity(), getChildFragmentManager(), R.layout.fragment_recipe_pager);
        //mTabHost.setBackgroundColor(Color.parseColor("#000000"));
        Bundle arg1 = new Bundle();
        arg1.putInt("Arg for Frag1", 1);
        mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator("Recipes"),
                RecipesFragment.class, arg1);

        Bundle arg2 = new Bundle();
        arg2.putInt("Arg for Frag2", 2);
        mTabHost.addTab(mTabHost.newTabSpec("2").setIndicator("Wine "),
                WineFragment.class, arg2);

        Bundle arg3 = new Bundle();
        arg2.putInt("Arg for Frag3", 3);
        mTabHost.addTab(mTabHost.newTabSpec("3").setIndicator("Profile"),
                WineFragment.class, arg3);
//       // mTabHost.getTabWidget().getChildAt(0).setT
//        setupTab("Recipes");
//        setupTab("Wine");
//        setupTab("Profile");
//        mTabHost.getTabWidget().setDividerDrawable(R.drawable.tab_divider);
        return mTabHost;
    }

    private void setupTab( final String tag) {
        View tabview = createTabView(mTabHost.getContext(), tag);

        if(tag.equalsIgnoreCase("recipes")) mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator(tabview), RecipesFragment.class, null);
        if(tag.equalsIgnoreCase("wine")) mTabHost.addTab(mTabHost.newTabSpec("2").setIndicator(tabview), WineFragment.class, null);
        if(tag.equalsIgnoreCase("Profile")) mTabHost.addTab(mTabHost.newTabSpec("3").setIndicator(tabview), ProfileFragment.class, null);
    }

    private static View createTabView(final Context context, final String text) {
        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
        TextView tv = (TextView) view.findViewById(R.id.tabsText);
        tv.setText(text);
        return view;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mTabHost = null;
    }
}
