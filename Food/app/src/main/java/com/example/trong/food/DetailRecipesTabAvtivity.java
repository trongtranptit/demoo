package com.example.trong.food;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.trong.fragment.DetailProfileFragment;
import com.example.trong.fragment.DetailRecipesFragment;
import com.example.trong.fragment.DetailWineFragment;
import com.example.trong.fragment.ProfileFragment;
import com.example.trong.fragment.RecipesFragment;
import com.example.trong.fragment.WineFragment;

/**
 * Created by trong on 7/14/2017.
 */

public class DetailRecipesTabAvtivity extends AppCompatActivity{
    FragmentTabHost mTabHost;
    Toolbar toolbar;
    TextView tv_tittle;
    ImageView imgNavi, imgIcon;
    ImageView ibtn_search;
    String name1, name2;
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_detail_recipes_tab);
        setSupportActionBar(toolbar);
   //     tv_tittle = (TextView) findViewById(R.id.tv_tittle);
        ibtn_search = (ImageView) findViewById(R.id.img_compose);
//        tv_tittle.setText("Recipes");
        ibtn_search.setImageResource(R.drawable.ic_compose);
       //imgNavi.setImageResource(R.drawable.ic_navigate_next_white_24dp);
        mTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), android.R.id.tabcontent);
        name1 = getIntent().getStringExtra("name1");
        name2 = getIntent().getStringExtra("name2");
//        setupTab("Recipes");
//        setupTab("Wine");
//        setupTab("Profile");
        Bundle bundle = new Bundle();
        bundle.putString("name1", name1);
        bundle.putString("name2",name2);
        mTabHost.addTab(mTabHost.newTabSpec("Recipes")
                        .setIndicator("Recipes"),
                DetailRecipesFragment.class, bundle);

        mTabHost.addTab(mTabHost.newTabSpec("Wine")
                        .setIndicator("Wine"),
                DetailWineFragment.class,null);
        mTabHost.addTab(mTabHost.newTabSpec("Profile")
                        .setIndicator("Profile"),
                DetailProfileFragment.class, null);
//        mTabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor(("#000000")));
//       // mTabHost.getTabWidget().getChildAt(0).set(R.drawable.tab_bg);
//        mTabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor(("#000000")));
//        mTabHost.getTabWidget().getChildAt(2).setBackgroundColor(Color.parseColor(("#000000")));
    }

    private void setupTab( final String tag) {
        View tabview = createTabView(mTabHost.getContext(), tag);
        Bundle bundle = new Bundle();
        bundle.putString("name1", name1);
        bundle.putString("name2",name2);
        if(tag.equalsIgnoreCase("recipes")) mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator(tabview), RecipesFragment.class, bundle);
        if(tag.equalsIgnoreCase("wine")) mTabHost.addTab(mTabHost.newTabSpec("2").setIndicator(tabview), WineFragment.class, null);
        if(tag.equalsIgnoreCase("Profile")) mTabHost.addTab(mTabHost.newTabSpec("3").setIndicator(tabview), ProfileFragment.class, null);
    }

    private static View createTabView(final Context context, final String text) {
        View view = LayoutInflater.from(context).inflate(R.layout.tabs_bg, null);
        TextView tv = (TextView) view.findViewById(R.id.tabsText);
        tv.setText(text);
        return view;
    }
}
