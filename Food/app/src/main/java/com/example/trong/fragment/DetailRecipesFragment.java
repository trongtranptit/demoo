package com.example.trong.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.trong.adapter.DetailPagerAdapter;
import com.example.trong.food.R;

/**
 * Created by trong on 7/14/2017.
 */

public class DetailRecipesFragment extends Fragment {
    ViewPager detaiPager;
    TextView tvdetailName1, tvdetailName2;
    TextView detailContent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detail_recipes,container,false);
        detaiPager = (ViewPager) v.findViewById(R.id.pagerDetail);
        tvdetailName1 = (TextView) v.findViewById(R.id.tvDetaiName1);
        tvdetailName2 = (TextView) v.findViewById(R.id.tvDetaiName2);
        detailContent = (TextView) v.findViewById(R.id.tvdetailContent);

        DetailPagerAdapter adapter = new DetailPagerAdapter(getContext());
        detaiPager.setAdapter(adapter);
        detailContent.setText(R.string.detail_content);
        tvdetailName1.setText(getArguments().getString("name1"));
        tvdetailName2.setText(getArguments().getString("name2"));
        return v;
    }
}
