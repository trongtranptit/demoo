package com.example.trong.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.trong.adapter.CategoryListAdapter;
import com.example.trong.food.R;
import com.example.trong.model.CategoryModel;

import java.util.ArrayList;

/**
 * Created by trong on 7/19/2017.
 */

public class CategoryFragment extends Fragment {
    RecyclerView recyclerView;
    ArrayList<CategoryModel> data = new ArrayList<CategoryModel>();
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_category,container,false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recyclerCate);
        addData();

//        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(),new );
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        CategoryListAdapter adapter = new CategoryListAdapter(v.getContext(),data);
        recyclerView.setAdapter(adapter);
        return v;
    }

    private void addData() {
        for(int i=0;i<4;i++){
            CategoryModel model = new CategoryModel("Bevarages","42");
            data.add(model);
            model = new CategoryModel("Breakfasts","87");
            data.add(model);
            model = new CategoryModel("Desserts","34");
            data.add(model);
            model = new CategoryModel("Main Dishes","94");
            data.add(model);
            model = new CategoryModel("Side Dishes","12");
            data.add(model);
            model = new CategoryModel("Appetizer","69");
            data.add(model);
        }
    }
}
