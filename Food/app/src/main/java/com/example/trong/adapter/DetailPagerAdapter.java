package com.example.trong.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.trong.food.R;

/**
 * Created by trong on 7/14/2017.
 */

public class DetailPagerAdapter extends PagerAdapter {
    int [] res={R.drawable.recipe1, R.drawable.recipe2,R.drawable.recipe1, R.drawable.recipe2,R.drawable.recipe1, R.drawable.recipe2};
    Context context;
    LayoutInflater inflater;
    public DetailPagerAdapter(Context context){
        Log.e("detail","tadaaa");
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return res.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View v = inflater.inflate(R.layout.detail_pager_item,container,false);
        ImageView img = (ImageView) v.findViewById(R.id.imgDetailPagerItem);
        img.setImageResource(res[position]);
        container.addView(v);
        return v;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
