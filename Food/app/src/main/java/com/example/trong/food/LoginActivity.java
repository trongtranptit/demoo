package com.example.trong.food;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by trong on 7/12/2017.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    ImageButton ibtn_fb, ibtn_tw;
    TextInputEditText edt_user, edt_pwd;
    Button btn_signin;
    CheckBox cbKeepSignIn;
    TextView tvForgotPwd, tvRegister;
    Button user,pwd;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
//        getSupportActionBar().hide();
        user = (Button) findViewById(R.id.btnUserIcon);
        pwd = (Button) findViewById(R.id.btnPwdIcon);
        ibtn_fb =(ImageButton) findViewById(R.id.ibtnFacebook);
        ibtn_tw =(ImageButton) findViewById(R.id.ibtnTwitter);
        edt_user = (TextInputEditText) findViewById(R.id.edtUser);
        edt_pwd = (TextInputEditText) findViewById(R.id.edtPassword);
        btn_signin = (Button) findViewById(R.id.btnSignIn);
        cbKeepSignIn = (CheckBox) findViewById(R.id.cbKeepSignIn);
        tvForgotPwd = (TextView) findViewById(R.id.tvForgotPwd);
        tvRegister = (TextView) findViewById(R.id.tvRegister);

        ibtn_tw.setOnClickListener(this);
        ibtn_fb.setOnClickListener(this);
        btn_signin.setOnClickListener(this);
        tvForgotPwd.setOnClickListener(this);
        tvRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        String user = edt_user.getText().toString();
        String pwd = edt_pwd.getText().toString();
        if(id == R.id.ibtnFacebook){
            login(user ,pwd);
            return;
        }
        if(id == R.id.ibtnTwitter){
            login(user,pwd);
            return;
        }
        if(id == R.id.btnSignIn){
            login(user,pwd);
            return;
        }
    }

    public void login(String user, String pwd){
        int status = verify(user, pwd);
        logInStatus(status);
        if(status ==2){
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
        }

    }

    public int verify(String user, String pwd){
        if(user.equals("")) return 1;
        if(pwd.equals("")) return 0;
        return 2;
    }

    public void logInStatus(int i){
        String s = "";
        if(i==0) s="Mat khau khong duoc de trong";
        else if(i==1) s="Ten dang nhap khong duoc de trong";
        else if(i==2) s="Dang nhap thanh cong";
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_SHORT).show();
    }
}
