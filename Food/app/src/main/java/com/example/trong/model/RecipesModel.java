package com.example.trong.model;

import java.io.Serializable;

/**
 * Created by trong on 7/14/2017.
 */

public class RecipesModel implements Serializable {
    private String name1, name2;

    public RecipesModel(String name1, String name2) {
        this.name1 = name1;
        this.name2 = name2;
    }

    public RecipesModel() {
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }
}
