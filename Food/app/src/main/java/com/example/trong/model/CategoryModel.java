package com.example.trong.model;

import java.io.Serializable;

/**
 * Created by trong on 7/19/2017.
 */

public class CategoryModel implements Serializable {
    private String name_item, number;
    private int imgRes;

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }

    public CategoryModel(String name_item, String number, int imgRes) {
        this.name_item = name_item;
        this.number = number;
        this.imgRes = imgRes;
    }

    public CategoryModel(String name_item, String number) {
        this.name_item = name_item;
        this.number = number;
    }

    public CategoryModel() {
    }

    public String getName_item() {
        return name_item;
    }

    public void setName_item(String name_item) {
        this.name_item = name_item;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
