package com.example.trong.food;

import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.trong.fragment.CategoryFragment;
import com.example.trong.fragment.DetailProfileFragment;
import com.example.trong.fragment.DetailRecipesFragment;
import com.example.trong.fragment.DetailWineFragment;
import com.example.trong.fragment.ProfileFragment;
import com.example.trong.fragment.RecipePagerFragment;
import com.example.trong.fragment.RecipesFragment;
import com.example.trong.fragment.WineFragment;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    FragmentTabHost mTabHost;
    TextView tv_tittle;
    ImageView imgNavi, imgIcon;
    ImageView ibtn_search;
    Toolbar toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        tv_tittle = (TextView) findViewById(R.id.tv_tittle);
        imgNavi = (ImageView)findViewById(R.id.imgNavi);
        imgIcon = (ImageView) findViewById(R.id.imgAppIcon);
        ibtn_search = (ImageView) findViewById(R.id.btn_search);
        tv_tittle.setText("Recipes");

       // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        getSupportActionBar().hide();
       // setSupportActionBar(toolbar);
        //getSupportActionBar().setLogo(R.drawable.ic_drawer);
//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer,toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setCheckedItem(R.id.recipes);
        navigationView.setItemIconTintList(null);

        mTabHost = (FragmentTabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.tabContenttt);

        mTabHost.addTab(mTabHost.newTabSpec("Recipes")
                        .setIndicator("Recipes"),
                RecipesFragment.class, null);

        mTabHost.addTab(mTabHost.newTabSpec("Wine")
                        .setIndicator("Wine"),
                WineFragment.class,null);
        mTabHost.addTab(mTabHost.newTabSpec("Profile")
                        .setIndicator("Profile"),
                ProfileFragment.class, null);
        //callFragment(new RecipesFragment());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.home) {
            tv_tittle.setText(R.string.home);
            // Handle the camera action
        } else if (id == R.id.compose) {
            tv_tittle.setText(R.string.compose);

        } else if (id == R.id.recipes) {
            tv_tittle.setText(R.string.recipes);

            callFragment(new RecipesFragment());

        } else if (id == R.id.category){
            tv_tittle.setText(R.string.category);
            callFragment(new CategoryFragment());

        } else if (id == R.id.setting) {
            tv_tittle.setText(R.string.setting);

        } else if (id == R.id.profile) {
            tv_tittle.setText(R.string.profile);

        } else if (id == R.id.sign_out){
           // tv_tittle.setText("Sing");

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void callFragment(Fragment fragment){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.tabContenttt, fragment);
        transaction.commit();
    }
}
