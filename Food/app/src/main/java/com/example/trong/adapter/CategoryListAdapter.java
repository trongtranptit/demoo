package com.example.trong.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.trong.food.DetailRecipesTabAvtivity;
import com.example.trong.food.R;
import com.example.trong.model.CategoryModel;

import java.util.ArrayList;

/**
 * Created by trong on 7/19/2017.
 */

public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.CategoryHolder> {
    Context ctx;
    ArrayList<CategoryModel> data;
    private int resImg[] ={R.drawable.cat1,
                        R.drawable.cat2,
                        R.drawable.cat3,
                        R.drawable.cat4,
                        R.drawable.cat5,
                        R.drawable.cat6};
    public CategoryListAdapter(Context ctx, ArrayList<CategoryModel> data){
        this.data= data;
        this.ctx = ctx;
        //addData();
    }
    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.category_list_item,parent,false);
        return new CategoryHolder(v);
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {
        holder.number_item.setText(data.get(position).getNumber());
        holder.tv_name_item.setText(data.get(position).getName_item());
        holder.img_category_item.setImageResource(resImg[position%6]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent t = new Intent(v.getContext(), DetailRecipesTabAvtivity.class);
                v.getContext().startActivity(t);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class CategoryHolder extends RecyclerView.ViewHolder {
        ImageView img_category_item;
        TextView tv_name_item, number_item;
        public CategoryHolder(View itemView) {
            super(itemView);

            img_category_item = (ImageView) itemView.findViewById(R.id.img_category_item);
            tv_name_item = (TextView) itemView.findViewById(R.id.tv_category_item);
            number_item = (TextView) itemView.findViewById(R.id.cirImg_dot);
            number_item.setBackgroundResource(R.drawable.dot);
        }
    }

    private void addData() {
        for(int i=0;i<4;i++){
            CategoryModel model = new CategoryModel("Bevarages","42",resImg[0]);
            data.add(model);
            model = new CategoryModel("Breakfasts","87",resImg[1]);
            data.add(model);
            model = new CategoryModel("Desserts","34",resImg[2]);
            data.add(model);
            model = new CategoryModel("Main Dishes","94",resImg[3]);
            data.add(model);
            model = new CategoryModel("Side Dishes","12",resImg[4]);
            data.add(model);
            model = new CategoryModel("Appetizer","69",resImg[5]);
            data.add(model);
        }
    }
}
