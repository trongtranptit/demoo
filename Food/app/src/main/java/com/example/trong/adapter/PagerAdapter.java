package com.example.trong.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.trong.fragment.ProfileFragment;
import com.example.trong.fragment.RecipesFragment;
import com.example.trong.fragment.WineFragment;

/**
 * Created by trong on 7/12/2017.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int numOfTabs;
    public PagerAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0: return new RecipesFragment();
            case 1: return new WineFragment();
            case 2: return new ProfileFragment();
        }
        return null;

    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
